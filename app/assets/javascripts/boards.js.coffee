# Allow dragging and dropping between columns
$ ->
  $columns = $('.column-items')
  $columns.sortable({
    connectWith: '.column-items',
    placeholder: 'placeholder',
    forceHelperSize: true,
    forcePlaceholderSize: true,
    cursor: 'move',
  })
  $columns.disableSelection()
