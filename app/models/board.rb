class Board < ActiveRecord::Base

  def columns
    {
      'backlog' => _('Backlog'),
      'doing'   => _('Doing'),
      'done'    => _('Done')
    }
  end

end
