class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.string  :identifier
      t.string  :name
      t.text    :columns

      t.timestamps
    end
  end
end
