require 'spec_helper'

describe Board do
  it 'has 3 columns when created' do
    b = Board.new
    expect(b.columns.size).to eq(3)
  end
end
