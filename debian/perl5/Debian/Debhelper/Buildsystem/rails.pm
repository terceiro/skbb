# A debhelper build system class for building Rails projects.
#
# Copyright: © 2015 Antonio Terceiro
# License: GPL-2+
# Strongly based on other build systems. Thanks!

package Debian::Debhelper::Buildsystem::rails;

use strict;
use base 'Debian::Debhelper::Buildsystem';

sub DESCRIPTION {
	"Rails"
}

sub check_auto_buildable {
	my $this=shift;
	my $result = (
		-e $this->get_sourcepath("Gemfile")
		&& -e $this->get_sourcepath('config.ru')
	);
	return $result;
}

sub new {
	my $class=shift;
	my $this=$class->SUPER::new(@_);
	$this->enforce_in_source_building();
	return $this;
}

sub configure {
	my $this=shift;
	$this->doit_in_sourcedir("dh_rails", "configure", @_);
}

sub build {
	my $this=shift;
	$this->doit_in_sourcedir("dh_rails", "build", @_);
}

sub test {
	my $this=shift;
	$this->doit_in_sourcedir("dh_rails", "test", @_);
}

sub install {
	my $this=shift;
	$this->doit_in_sourcedir("dh_rails", "install", @_);
}

sub clean {
	my $this=shift;
	$this->doit_in_sourcedir("dh_rails", "clean", @_);
}

1
